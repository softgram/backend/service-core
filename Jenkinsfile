pipeline {
    agent {
        label 'docker'
    }
    environment {
        GITLAB_USERNAME = "${env.GITLAB_USER_USERNAME}"
        GITLAB_PASSWORD = "${env.GITLAB_USER_PASSWORD}"
    }

    stages {
        stage('Preparation') {
           steps {
               script {
                    def runningPipelinesCount = 1

                    while (runningPipelinesCount > 0) {
                        def response = bat(script: 'curl -o result.txt --request GET --url "https://${GITLAB_USERNAME}:${GITLAB_PASSWORD}@gitlab.com/api/v4/projects/51613880/pipelines?status=running"',  returnStdout: true).trim()
                        def json = readFile('result.txt')
                        def lista = new groovy.json.JsonSlurperClassic().parseText(json)
                        runningPipelinesCount = lista.size()

                        if (runningPipelinesCount == 0) {
                            echo "Nothing pipelines in execute. Next..."
                        } else {
                            echo "Have ${runningPipelinesCount} pipelines in execute. waiting 5 seconds before the returning verify..."
                            sleep(time: 5, unit: 'SECONDS')
                        }
                    }

                   bat "git config --global submodule.recurse false"

                   git branch: 'main', credentialsId: 'integration_gitlab_jenkins', url: 'https://gitlab.com/softgram/backend/root.git'

                   bat "git submodule init"
                   bat "git submodule update --init --recursive -- context/service/service-core"

                   dir("context/service") {
                        bat(script: 'powershell.exe -Command "Get-ChildItem | Where-Object { $_.Name -ne \\\"service-core\\\" } | ForEach-Object { Remove-Item -Path $_.FullName -Recurse -Force }"')
                   }

                   dir("settings") {
                       git branch: 'main', credentialsId: 'integration_gitlab_jenkins', url: 'https://gitlab.com/softgram/backend/docker-compose.git'
                  }
               }
           }
       }

        stage('Build') {
            steps {
                script {
                    bat "docker pull gradle:jdk17-graal-jammy"

                    def gradleCmd = 'gradle'
                    def dockerCmd = "docker run --rm -v ${pwd()}:/workspace -w /workspace gradle:jdk17-graal-jammy $gradleCmd clean build"

                    bat(script: dockerCmd, returnStatus: true)
                }
            }
        }
        stage('Running') {
            steps {
                script {
                    dir('settings') {
                        try {
                            bat 'docker-compose down service-core'
                            bat 'docker-compose up -d service-core'
                        } catch (Exception e) {
                            bat 'docker-compose up -d service-core'
                        }
                    }
                }
            }
        }
    }
     post {
        success {
            echo 'Application Success'
        }
    }
}