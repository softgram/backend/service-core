package co.com.project.core.shared.domain.exception;

import co.com.project.common.domain.enums.ErrorStatus;
import co.com.project.common.domain.exception.ApplicationException;
import lombok.experimental.StandardException;

@StandardException
public class ClientRequestException extends ApplicationException {

    public ClientRequestException(String message) {
        super(message, ErrorStatus.BAD_REQUEST);
    }
}
