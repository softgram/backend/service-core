package co.com.project.core.shared.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(builderMethodName = "init")
public class Base {
    private Long id;
    private LocalDate createAt;
    private LocalDate updateAt;
    private Boolean status;
}
