package co.com.project.core.shared.adapter.inbound.web.dto;

import jakarta.validation.constraints.Null;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(builderMethodName = "init")
public class EntityBaseDTO {
    @Null
    private Long id;
    @Null
    private LocalDate createAt;
    @Null
    private LocalDate updateAt;
    @Null
    private Boolean status;
}
