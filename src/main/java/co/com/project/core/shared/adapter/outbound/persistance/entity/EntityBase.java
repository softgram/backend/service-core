package co.com.project.core.shared.adapter.outbound.persistance.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Getter
@Setter
@MappedSuperclass
public class EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "CREATE_AT")
    @JsonFormat(pattern = "dd-MM-yyy")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate createAt;

    @Column(name = "UPDATE_AT")
    @JsonFormat(pattern = "dd-MM-yyy")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate updateAt;

    @Column(name = "STATUS")
    private Boolean status;

    @PrePersist
    public void prePersist() {
        this.createAt = LocalDate.now();
        this.updateAt = LocalDate.now();
        this.status = true;
    }

    @PreUpdate
    public void preUpdate() {
        this.updateAt = LocalDate.now();
    }
}
