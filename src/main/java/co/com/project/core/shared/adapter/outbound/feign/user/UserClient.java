package co.com.project.core.shared.adapter.outbound.feign.user;

import co.com.project.common.adapter.inbound.web.dto.Response;
import co.com.project.core.shared.adapter.inbound.web.dto.UserDTO;
import co.com.project.core.shared.configuration.FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "service-security", path = "/api/users", configuration = FeignConfiguration.class)
public interface UserClient {
    @GetMapping("/{id}")
    Response<UserDTO> findById(@PathVariable Long id);
}
