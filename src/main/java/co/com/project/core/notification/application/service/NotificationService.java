package co.com.project.core.notification.application.service;

import co.com.project.core.notification.application.port.NotificationUseCase;
import co.com.project.core.notification.domain.model.Notification;
import co.com.project.core.notification.domain.repository.NotificationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NotificationService implements NotificationUseCase {
    private final NotificationRepository repository;

    @Override
    public Page<Notification> findAllByUser(Long userId, Pageable pageable) {
        return repository.findAllByUser(userId, pageable);
    }

    @Override
    public Notification saveNotification(Notification notification) {
        return repository.saveNotification(notification);
    }
}
