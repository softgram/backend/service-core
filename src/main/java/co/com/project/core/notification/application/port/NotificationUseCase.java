package co.com.project.core.notification.application.port;

import co.com.project.core.notification.domain.model.Notification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface NotificationUseCase {
    Page<Notification> findAllByUser(Long userId, Pageable pageable);
    Notification saveNotification(Notification notification);
}
