package co.com.project.core.notification.adapter.outbound.persistance.entity;

import co.com.project.core.shared.adapter.outbound.persistance.entity.EntityBase;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "NOTIFICATION")
public class Notification extends EntityBase {
    @Column(name = "USER_ID")
    private Long userId;
    @Column(name = "OTHER_ID")
    private Long otherId;
}
