package co.com.project.core.notification.adapter.inbound.mapper;

import co.com.project.core.notification.adapter.inbound.web.dto.NotificationDTO;
import co.com.project.core.notification.domain.model.Notification;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NotificationMapStructDTO {
    Notification toDomain(NotificationDTO notification);
    @InheritInverseConfiguration(name = "toDomain")
    NotificationDTO toDTO(Notification notification);
}
