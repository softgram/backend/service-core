package co.com.project.core.notification.domain.exception;

import co.com.project.common.domain.exception.NotFoundException;
import lombok.experimental.StandardException;

@StandardException
public class NotNotificationsByUserException extends NotFoundException {
    public NotNotificationsByUserException(String message) {
        super("Notifications not found by user ".concat(message));
    }
}
