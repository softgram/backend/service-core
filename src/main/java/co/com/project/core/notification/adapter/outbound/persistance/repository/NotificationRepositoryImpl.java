package co.com.project.core.notification.adapter.outbound.persistance.repository;

import co.com.project.core.notification.adapter.outbound.mapper.NotificationMapStructEntity;
import co.com.project.core.notification.adapter.outbound.persistance.repository.jpa.JpaNotificationRepository;
import co.com.project.core.notification.domain.model.Notification;
import co.com.project.core.notification.domain.repository.NotificationRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class NotificationRepositoryImpl implements NotificationRepository {
    @PersistenceContext private final EntityManager em;
    private final JpaNotificationRepository repository;
    private final NotificationMapStructEntity mapper;


    @Override
    public Page<Notification> findAllByUser(Long userId, Pageable pageable) {
        return repository.findAllByUserId(userId, pageable).map(mapper::toDomain);
    }

    @Override
    public Notification saveNotification(Notification notification) {
        return mapper.toDomain(em.merge(mapper.toEntity(notification)));
    }
}
