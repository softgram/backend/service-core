package co.com.project.core.notification.adapter.inbound.web.controller;

import co.com.project.common.adapter.inbound.web.dto.Response;
import co.com.project.core.notification.adapter.inbound.mapper.NotificationMapStructDTO;
import co.com.project.core.notification.adapter.inbound.web.dto.NotificationDTO;
import co.com.project.core.notification.application.port.NotificationUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import static co.com.project.common.adapter.util.ResponseBuilder.success;

@RestController
@RequiredArgsConstructor
@RequestMapping("/notifications")
@CrossOrigin(originPatterns = {"http://localhost:4200"})
public class NotificationController {
    private final NotificationUseCase service;
    private final NotificationMapStructDTO mapper;

    @GetMapping("/{id}")
    public Response<Page<NotificationDTO>> findAllByUser(@PathVariable Long id, Pageable pageable) {
        return success(service.findAllByUser(id, pageable).map(mapper::toDTO));
    }

    @PostMapping
    public Response<NotificationDTO> saveNotification(@RequestBody NotificationDTO notification) {
        return success(mapper.toDTO(service.saveNotification(mapper.toDomain(notification))));
    }
}
