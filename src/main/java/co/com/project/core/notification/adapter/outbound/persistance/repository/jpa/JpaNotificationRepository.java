package co.com.project.core.notification.adapter.outbound.persistance.repository.jpa;

import co.com.project.core.notification.adapter.outbound.persistance.entity.Notification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaNotificationRepository extends JpaRepository<Notification, Long> {
    Page<Notification> findAllByUserId(Long id, Pageable pageable);
    Page<Notification> findAllByOtherId(Long id, Pageable pageable);
}
