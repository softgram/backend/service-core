package co.com.project.core.notification.adapter.outbound.mapper;

import co.com.project.core.notification.domain.model.Notification;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NotificationMapStructEntity {
    Notification toDomain(co.com.project.core.notification.adapter.outbound.persistance.entity.Notification notification);
    @InheritInverseConfiguration(name = "toDomain")
    co.com.project.core.notification.adapter.outbound.persistance.entity.Notification toEntity(Notification notification);
}
