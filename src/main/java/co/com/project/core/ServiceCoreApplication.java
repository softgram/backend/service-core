package co.com.project.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication(
	scanBasePackages = {
		"co.com.project.common",
		"co.com.project.core",
		"co.com.project.s3"
	}
)
public class ServiceCoreApplication {

	public static void main(String... args) {
		SpringApplication.run(ServiceCoreApplication.class);
	}

}
