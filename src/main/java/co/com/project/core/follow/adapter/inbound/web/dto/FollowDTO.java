package co.com.project.core.follow.adapter.inbound.web.dto;

import co.com.project.core.shared.adapter.inbound.web.dto.EntityBaseDTO;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder(builderMethodName = "init")
public class FollowDTO extends EntityBaseDTO {
    @NotNull
    @NotEmpty
    private Long userId;
    @NotNull
    @NotEmpty
    private Long followerId;
}
