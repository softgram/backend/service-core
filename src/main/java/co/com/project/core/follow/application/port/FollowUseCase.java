package co.com.project.core.follow.application.port;

import co.com.project.core.follow.domain.model.Follow;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface FollowUseCase {
    Page<Follow> findFollowersByFollowerId(Long id, Pageable pageable);
    Page<Follow> findFollowersByUserId(Long id, Pageable pageable);
    Follow saveFollow(Follow follow);
}
