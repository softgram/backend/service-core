package co.com.project.core.follow.adapter.inbound.mapper;

import co.com.project.core.follow.adapter.inbound.web.dto.FollowDTO;
import co.com.project.core.follow.domain.model.Follow;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface FollowMapStructDTO {
    Follow toDomain(FollowDTO follow);
    @InheritInverseConfiguration(name = "toDomain")
    FollowDTO toDTO(Follow follow);
}
