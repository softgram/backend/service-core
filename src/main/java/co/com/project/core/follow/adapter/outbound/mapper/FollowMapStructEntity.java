package co.com.project.core.follow.adapter.outbound.mapper;

import co.com.project.core.follow.domain.model.Follow;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface FollowMapStructEntity {
    Follow toDomain(co.com.project.core.follow.adapter.outbound.persistance.entity.Follow follow);
    @InheritInverseConfiguration(name = "toDomain")
    co.com.project.core.follow.adapter.outbound.persistance.entity.Follow toEntity(Follow follow);
}
