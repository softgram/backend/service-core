package co.com.project.core.follow.adapter.outbound.persistance.entity;

import co.com.project.core.shared.adapter.outbound.persistance.entity.EntityBase;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "FOLLOW")
public class Follow extends EntityBase {
    @Column(name = "FOLLOWER_ID")
    private Long followerId;
    @Column(name = "USER_ID")
    private Long userId;
}
