package co.com.project.core.follow.adapter.inbound.web.controller;

import co.com.project.common.adapter.inbound.web.dto.Response;
import co.com.project.core.follow.adapter.inbound.mapper.FollowMapStructDTO;
import co.com.project.core.follow.adapter.inbound.web.dto.FollowDTO;
import co.com.project.core.follow.application.port.FollowUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import static co.com.project.common.adapter.util.ResponseBuilder.success;

@RestController
@RequiredArgsConstructor
@RequestMapping("/follows")
@CrossOrigin(originPatterns = {"http://localhost:4200"})
public class FollowController {
    private final FollowUseCase service;
    private final FollowMapStructDTO mapper;

    @GetMapping("/follower/{id}")
    public Response<Page<FollowDTO>> findByFollower(@PathVariable Long id, Pageable pageable) {
        return success(service.findFollowersByFollowerId(id, pageable).map(mapper::toDTO));
    }

    @GetMapping("/user/{id}")
    public Response<Page<FollowDTO>> findByUser(@PathVariable Long id, Pageable pageable) {
        return success(service.findFollowersByUserId(id, pageable).map(mapper::toDTO));
    }

    @PostMapping
    public Response<FollowDTO> save(@RequestBody FollowDTO follow) {
        return success(mapper.toDTO(service.saveFollow(mapper.toDomain(follow))));
    }
}
