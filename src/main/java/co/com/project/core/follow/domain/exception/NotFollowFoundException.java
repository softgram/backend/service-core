package co.com.project.core.follow.domain.exception;

import co.com.project.common.domain.exception.NotFoundException;
import lombok.experimental.StandardException;

@StandardException
public class NotFollowFoundException extends NotFoundException {
    public NotFollowFoundException(String message) {
        super("Not found followers by ".concat(message));
    }
}
