package co.com.project.core.follow.adapter.outbound.persistance.repository.jpa;

import co.com.project.core.follow.adapter.outbound.persistance.entity.Follow;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaFollowRepository extends JpaRepository<Follow, Long> {
    Page<Follow> findAllByUserId(Long id, Pageable pageable);
    Page<Follow> findAllByFollowerId(Long id, Pageable pageable);
}
