package co.com.project.core.follow.adapter.outbound.persistance.repository;

import co.com.project.core.follow.adapter.outbound.mapper.FollowMapStructEntity;
import co.com.project.core.follow.adapter.outbound.persistance.repository.jpa.JpaFollowRepository;
import co.com.project.core.follow.domain.model.Follow;
import co.com.project.core.follow.domain.repository.FollowRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class FollowRepositoryImpl implements FollowRepository {
    @PersistenceContext private final EntityManager em;
    private final FollowMapStructEntity mapper;
    private final JpaFollowRepository repository;

    @Override
    public Page<Follow> findFollowersByFollowerId(Long id, Pageable pageable) {
        return repository.findAllByFollowerId(id, pageable).map(mapper::toDomain);
    }

    @Override
    public Page<Follow> findFollowersByUserId(Long id, Pageable pageable) {
        return repository.findAllByUserId(id, pageable).map(mapper::toDomain);
    }

    @Override
    public Follow saveFollow(Follow follow) {
        return mapper.toDomain(em.merge(mapper.toEntity(follow)));
    }
}
