package co.com.project.core.follow.application.service;

import co.com.project.core.follow.application.port.FollowUseCase;
import co.com.project.core.follow.domain.model.Follow;
import co.com.project.core.follow.domain.repository.FollowRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FollowService implements FollowUseCase {
    private final FollowRepository repository;

    @Override
    public Page<Follow> findFollowersByFollowerId(Long id, Pageable pageable) {
        return repository.findFollowersByFollowerId(id, pageable);
    }

    @Override
    public Page<Follow> findFollowersByUserId(Long id, Pageable pageable) {
        return repository.findFollowersByUserId(id, pageable);
    }

    @Override
    public Follow saveFollow(Follow follow) {
        return repository.saveFollow(follow);
    }
}
