package co.com.project.core.post.application.service;

import co.com.project.core.post.application.port.PostUseCase;
import co.com.project.core.post.domain.model.Post;
import co.com.project.core.post.domain.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Service
@RequiredArgsConstructor
public class PostService implements PostUseCase {
    private final PostRepository repository;

    @Override
    public Post save(Post post, MultipartFile file) {
        return repository.save(post, file);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Post findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Page<Post> findAll(Long id, Pageable pageable) {
        return repository.findAll(id, pageable);
    }
}
