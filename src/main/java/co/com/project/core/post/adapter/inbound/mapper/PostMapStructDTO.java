package co.com.project.core.post.adapter.inbound.mapper;

import co.com.project.core.post.adapter.inbound.web.dto.PostDTO;
import co.com.project.core.post.domain.model.Post;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PostMapStructDTO {
    @Mapping(source = "user.id", target = "userId")
    Post toDomain(PostDTO post);
    @Mapping(source = "userId", target = "user.id")
    @InheritInverseConfiguration(name = "toDomain")
    PostDTO toDTO(Post post);
}
