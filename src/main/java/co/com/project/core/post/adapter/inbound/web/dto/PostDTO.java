package co.com.project.core.post.adapter.inbound.web.dto;

import co.com.project.core.comment.adapter.inbound.web.dto.CommentDTO;
import co.com.project.core.like.adapter.inbound.web.dto.LikeDTO;
import co.com.project.core.shared.adapter.inbound.web.dto.EntityBaseDTO;
import co.com.project.core.shared.adapter.inbound.web.dto.UserDTO;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder(builderMethodName = "init")
public class PostDTO extends EntityBaseDTO {
    @NotNull
    @NotBlank
    private String name;
    @NotNull
    @NotBlank
    private String title;
    @NotNull
    @NotBlank
    private String description;
    @NotNull
    @NotBlank
    private String url;
    @NotNull
    private UserDTO user;
    @Null
    private List<CommentDTO> comments;
    @Null
    private List<LikeDTO> likes;
}
