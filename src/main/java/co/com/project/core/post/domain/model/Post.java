package co.com.project.core.post.domain.model;

import co.com.project.core.comment.domain.model.Comment;
import co.com.project.core.like.domain.model.Like;
import co.com.project.core.shared.domain.model.Base;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder(builderMethodName = "init")
public class Post extends Base {
    private String name;
    private String title;
    private String description;
    private String url;
    private Long userId;
    private List<Comment> comments;
    private List<Like> likes;
}
