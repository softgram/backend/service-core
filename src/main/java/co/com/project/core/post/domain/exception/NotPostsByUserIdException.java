package co.com.project.core.post.domain.exception;

import co.com.project.common.domain.exception.NotFoundException;
import lombok.experimental.StandardException;

@StandardException
public class NotPostsByUserIdException extends NotFoundException {

    public NotPostsByUserIdException() {
        super("Not found posts by user id in server.");
    }
}
