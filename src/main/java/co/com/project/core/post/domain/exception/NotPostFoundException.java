package co.com.project.core.post.domain.exception;

import co.com.project.common.domain.exception.NotFoundException;
import lombok.experimental.StandardException;

@StandardException
public class NotPostFoundException extends NotFoundException {
    public NotPostFoundException() {
        super("Not post found in server.");
    }
}
