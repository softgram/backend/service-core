package co.com.project.core.post.application.port;

import co.com.project.core.post.domain.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public interface PostUseCase {
    Post save(Post post, MultipartFile file);
    void deleteById(Long id);
    Post findById(Long id);
    Page<Post> findAll(Long id, Pageable pageable);
}
