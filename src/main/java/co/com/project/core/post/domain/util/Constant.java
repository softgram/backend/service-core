package co.com.project.core.post.domain.util;

import lombok.Getter;
import lombok.experimental.UtilityClass;

@Getter
@UtilityClass
public class Constant {
    public static final String BUCKET_POST = "posts";
}
