package co.com.project.core.post.adapter.outbound.persistance.entity;

import co.com.project.core.comment.adapter.outbound.persistance.entity.Comment;
import co.com.project.core.like.adapter.outbound.persistance.entity.Like;
import co.com.project.core.shared.adapter.outbound.persistance.entity.EntityBase;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "POST")
public class Post extends EntityBase {
    @Column(name = "NAME")
    private String name;
    @Column(name = "TITLE")
    private String title;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "URL")
    private String url;
    @Column(name = "USER_ID")
    private Long userId;
    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Comment> comments;
    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Like> likes;
}
