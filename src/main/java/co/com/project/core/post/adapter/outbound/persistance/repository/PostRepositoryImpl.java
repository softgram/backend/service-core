package co.com.project.core.post.adapter.outbound.persistance.repository;

import co.com.project.core.post.adapter.outbound.mapper.PostMapStructEntity;
import co.com.project.core.post.adapter.outbound.persistance.repository.jpa.JpaPostRepository;
import co.com.project.core.post.domain.exception.NotPostFoundException;
import co.com.project.core.post.domain.exception.NotPostsByUserIdException;
import co.com.project.core.post.domain.model.Post;
import co.com.project.core.post.domain.repository.PostRepository;
import co.com.project.core.post.domain.util.Constant;
import co.com.project.s3.application.port.S3UseCase;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Repository
@RequiredArgsConstructor
public class PostRepositoryImpl implements PostRepository {
    @PersistenceContext private final EntityManager manager;
    private final JpaPostRepository repository;
    private final PostMapStructEntity mapper;
    private final S3UseCase s3Service;

    @Override
    @Transactional
    public Post save(Post post, MultipartFile file) {
        s3Service.upload(Constant.BUCKET_POST, file);
        co.com.project.core.post.adapter.outbound.persistance.entity.Post postEntity = mapper.toEntity(post);
        postEntity.setUrl(s3Service.getUrl(Constant.BUCKET_POST, file.getOriginalFilename()));
        return mapper.toDomain(manager.merge(postEntity));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Post findById(Long id) {
        return repository
                    .findById(id)
                        .map(mapper::toDomain)
                            .orElseThrow(NotPostFoundException::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Post> findAll(Long id, Pageable pageable) {
        Page<Post> posts = repository
                                .findAllByUserId(id, pageable)
                                    .map(mapper::toDomain);

        if(posts.getTotalElements() == 0) throw new NotPostsByUserIdException();

        return posts;
    }
}
