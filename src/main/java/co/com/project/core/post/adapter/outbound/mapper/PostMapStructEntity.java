package co.com.project.core.post.adapter.outbound.mapper;

import co.com.project.core.post.domain.model.Post;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PostMapStructEntity {
    Post toDomain(co.com.project.core.post.adapter.outbound.persistance.entity.Post post);
    @InheritInverseConfiguration(name = "toDomain")
    co.com.project.core.post.adapter.outbound.persistance.entity.Post toEntity(Post post);
}
