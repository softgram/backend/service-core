package co.com.project.core.post.adapter.inbound.web.controller;

import co.com.project.common.adapter.inbound.web.dto.Response;
import co.com.project.core.post.adapter.inbound.mapper.PostMapStructDTO;
import co.com.project.core.post.adapter.inbound.web.dto.PostDTO;
import co.com.project.core.post.application.port.PostUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

import static co.com.project.common.adapter.util.ResponseBuilder.success;

@RestController
@RequiredArgsConstructor
@RequestMapping("/posts")
@CrossOrigin(originPatterns = {"http://localhost:4200"})
public class PostController {
    private final PostUseCase service;
    private final PostMapStructDTO mapper;

    @GetMapping("/{id}")
    public Response<Page<PostDTO>> findAll(@PathVariable Long id, Pageable pageable) {
        return success(service.findAll(id, pageable).map(mapper::toDTO));
    }

    @GetMapping("/id/{id}")
    public Response<PostDTO> findById(@PathVariable Long id) {
        return success(mapper.toDTO(service.findById(id)));
    }

    @PostMapping
    public Response<PostDTO> save(@RequestBody PostDTO post, @RequestParam("file") MultipartFile file) {
        return success(mapper.toDTO(service.save(mapper.toDomain(post), file)));
    }

    @DeleteMapping("/{id}")
    public Response<Void> deleteById(@PathVariable Long id) {
        return success(() -> service.deleteById(id));
    }
}
