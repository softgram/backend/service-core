package co.com.project.core.chat.domain.exception;

import co.com.project.common.domain.exception.NotFoundException;
import lombok.experimental.StandardException;

@StandardException
public class NotChatFoundException extends NotFoundException {
    public NotChatFoundException(String message) {
        super("Not found chat by ".concat(message));
    }
}
