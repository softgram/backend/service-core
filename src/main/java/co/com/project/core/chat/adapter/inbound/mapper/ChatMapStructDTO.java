package co.com.project.core.chat.adapter.inbound.mapper;

import co.com.project.core.chat.adapter.inbound.web.dto.ChatDTO;
import co.com.project.core.chat.domain.model.Chat;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ChatMapStructDTO {
    Chat toDomain(ChatDTO chat);
    ChatDTO toDTO(Chat chat);
}
