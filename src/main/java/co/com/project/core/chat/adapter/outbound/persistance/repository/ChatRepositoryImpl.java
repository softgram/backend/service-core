package co.com.project.core.chat.adapter.outbound.persistance.repository;

import co.com.project.core.chat.adapter.outbound.mapper.ChatMapStructEntity;
import co.com.project.core.chat.adapter.outbound.persistance.repository.jpa.JpaChatRepository;
import co.com.project.core.chat.domain.exception.NotChatFoundException;
import co.com.project.core.chat.domain.model.Chat;
import co.com.project.core.chat.domain.repository.ChatRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class ChatRepositoryImpl implements ChatRepository {
    @PersistenceContext private final EntityManager em;
    private final ChatMapStructEntity mapper;
    private final JpaChatRepository repository;

    @Override
    public Page<Chat> findAllByUser(Long userId, Pageable pageable) {
        return repository.findAllByUserId(userId, pageable).map(mapper::toDomain);
    }

    @Override
    public Chat findByUserIdAndOtherId(Long userId, Long otherId) {
        return repository
                    .findByUserIdAndOtherId(userId, otherId)
                        .map(mapper::toDomain)
                            .orElseThrow(NotChatFoundException::new);
    }

    @Override
    public Chat saveChat(Chat chat) {
        return mapper.toDomain(em.merge(mapper.toEntity(chat)));
    }
}
