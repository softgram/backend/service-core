package co.com.project.core.chat.adapter.outbound.persistance.entity;


import co.com.project.core.message.adapter.outbound.persistance.entity.Message;
import co.com.project.core.shared.adapter.outbound.persistance.entity.EntityBase;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "CHAT")
public class Chat extends EntityBase {
    @Column(name = "USER_ID")
    private Long userId;
    @Column(name = "OTHER_ID")
    private Long otherId;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "chat")
    private List<Message> messages;
}
