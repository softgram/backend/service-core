package co.com.project.core.chat.adapter.outbound.persistance.repository.jpa;

import co.com.project.core.chat.adapter.outbound.persistance.entity.Chat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface JpaChatRepository extends JpaRepository<Chat, Long> {
    Page<Chat> findAllByUserId(Long userId, Pageable pageable);
    Optional<Chat> findByUserIdAndOtherId(Long userId, Long otherId);
}
