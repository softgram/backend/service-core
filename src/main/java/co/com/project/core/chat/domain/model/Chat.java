package co.com.project.core.chat.domain.model;

import co.com.project.core.message.domain.model.Message;
import co.com.project.core.shared.domain.model.Base;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder(builderMethodName = "init")
public class Chat extends Base {
    private Long userId;
    private Long otherId;
    private List<Message> messages;
}
