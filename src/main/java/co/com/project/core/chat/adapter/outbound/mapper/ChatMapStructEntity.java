package co.com.project.core.chat.adapter.outbound.mapper;

import co.com.project.core.chat.domain.model.Chat;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ChatMapStructEntity {
    Chat toDomain(co.com.project.core.chat.adapter.outbound.persistance.entity.Chat chat);
    @InheritInverseConfiguration(name = "toDomain")
    co.com.project.core.chat.adapter.outbound.persistance.entity.Chat toEntity(Chat chat);
}
