package co.com.project.core.chat.application.port;

import co.com.project.core.chat.domain.model.Chat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ChatUseCase {
    Page<Chat> findAllByUser(Long userId, Pageable pageable);
    Chat findByUserIdAndOtherId(Long userId, Long otherId);
    Chat saveChat(Chat chat);

}
