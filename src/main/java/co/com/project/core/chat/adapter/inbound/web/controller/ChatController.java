package co.com.project.core.chat.adapter.inbound.web.controller;

import co.com.project.common.adapter.inbound.web.dto.Response;
import co.com.project.core.chat.adapter.inbound.mapper.ChatMapStructDTO;
import co.com.project.core.chat.adapter.inbound.web.dto.ChatDTO;
import co.com.project.core.chat.application.port.ChatUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import static co.com.project.common.adapter.util.ResponseBuilder.success;

@RestController
@RequiredArgsConstructor
@RequestMapping("/chats")
@CrossOrigin(originPatterns = {"http://localhost:4200"})
public class ChatController {
    private final ChatUseCase service;
    private final ChatMapStructDTO mapper;

    @GetMapping("/{id}")
    public Response<Page<ChatDTO>> findAllByUserId(@PathVariable Long id, Pageable pageable) {
        return success(service.findAllByUser(id, pageable).map(mapper::toDTO));
    }

    @GetMapping("/user/{id}/{otherId}")
    public Response<ChatDTO> findAllByUserIdAndOtherId(@PathVariable Long id, @PathVariable Long otherId) {
        return success(mapper.toDTO(service.findByUserIdAndOtherId(id, otherId)));
    }

    @PostMapping
    public Response<ChatDTO> saveChat(@RequestBody ChatDTO chat) {
        return success(mapper.toDTO(service.saveChat(mapper.toDomain(chat))));
    }
}
