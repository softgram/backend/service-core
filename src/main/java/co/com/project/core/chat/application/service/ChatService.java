package co.com.project.core.chat.application.service;

import co.com.project.core.chat.application.port.ChatUseCase;
import co.com.project.core.chat.domain.model.Chat;
import co.com.project.core.chat.domain.repository.ChatRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ChatService implements ChatUseCase {
    private final ChatRepository repository;

    @Override
    public Page<Chat> findAllByUser(Long userId, Pageable pageable) {
        return repository.findAllByUser(userId, pageable);
    }

    @Override
    public Chat findByUserIdAndOtherId(Long userId, Long otherId) {
        return repository.findByUserIdAndOtherId(userId, otherId);
    }

    @Override
    public Chat saveChat(Chat chat) {
        return repository.saveChat(chat);
    }
}
