package co.com.project.core.like.adapter.inbound.web.controller;

import co.com.project.common.adapter.inbound.web.dto.Response;
import co.com.project.core.like.adapter.inbound.mapper.LikeMapStructDTO;
import co.com.project.core.like.adapter.inbound.web.dto.LikeDTO;
import co.com.project.core.like.application.port.LikeUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import static co.com.project.common.adapter.util.ResponseBuilder.success;

@RestController
@RequiredArgsConstructor
@RequestMapping("/likes")
@CrossOrigin(originPatterns = {"http://localhost:4200"})
public class LikeController {
    private final LikeUseCase service;
    private final LikeMapStructDTO mapper;

    @GetMapping("/comments/{id}")
    public Response<Page<LikeDTO>> findAllByComment(@PathVariable Long id, Pageable pageable) {
        return success(service.findAllByCommentId(id, pageable).map(mapper::toDTO));
    }

    @GetMapping("/posts/{id}")
    public Response<Page<LikeDTO>> findAllByPost(@PathVariable Long id, Pageable pageable) {
        return success(service.findAllByPostId(id, pageable).map(mapper::toDTO));
    }

    @PostMapping
    public Response<LikeDTO> save(@RequestBody LikeDTO like) {
        return success(mapper.toDTO(service.save(mapper.toDomain(like))));
    }
}
