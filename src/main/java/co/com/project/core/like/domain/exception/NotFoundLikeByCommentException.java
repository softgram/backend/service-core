package co.com.project.core.like.domain.exception;

import co.com.project.common.domain.exception.NotFoundException;
import lombok.experimental.StandardException;

@StandardException
public class NotFoundLikeByCommentException extends NotFoundException {

    public NotFoundLikeByCommentException(String message) {
        super("Not found like by comment " + message + " in server");
    }
}
