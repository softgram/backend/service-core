package co.com.project.core.like.adapter.inbound.mapper;

import co.com.project.core.like.adapter.inbound.web.dto.LikeDTO;
import co.com.project.core.like.domain.model.Like;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface LikeMapStructDTO {
    @Mapping(source = "user.id", target = "userId")
    Like toDomain(LikeDTO like);
    @Mapping(source = "userId", target = "user.id")
    @InheritInverseConfiguration(name = "toDomain")
    LikeDTO toDTO(Like like);
}
