package co.com.project.core.like.adapter.outbound.mapper;

import co.com.project.core.like.domain.model.Like;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LikeMapStructEntity {
    Like toDomain(co.com.project.core.like.adapter.outbound.persistance.entity.Like like);
    @InheritInverseConfiguration(name = "toDomain")
    co.com.project.core.like.adapter.outbound.persistance.entity.Like toEntity(Like like);
}
