package co.com.project.core.like.application.service;

import co.com.project.core.like.application.port.LikeUseCase;
import co.com.project.core.like.domain.model.Like;
import co.com.project.core.like.domain.repository.LikeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LikeService implements LikeUseCase {
    private final LikeRepository repository;

    @Override
    public Page<Like> findAllByCommentId(Long id, Pageable pageable) {
        return repository.findAllByCommentId(id, pageable);
    }

    @Override
    public Page<Like> findAllByPostId(Long id, Pageable pageable) {
        return repository.findAllByPostId(id, pageable);
    }

    @Override
    public Like save(Like like) {
        return repository.save(like);
    }
}
