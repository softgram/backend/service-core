package co.com.project.core.like.adapter.outbound.persistance.repository;

import co.com.project.core.like.adapter.outbound.mapper.LikeMapStructEntity;
import co.com.project.core.like.adapter.outbound.persistance.repository.jpa.JpaLikeRepository;
import co.com.project.core.like.domain.model.Like;
import co.com.project.core.like.domain.repository.LikeRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class LikeRepositoryImpl implements LikeRepository {
    @PersistenceContext private final EntityManager manager;
    private final JpaLikeRepository repository;
    private final LikeMapStructEntity mapper;

    @Override
    public Page<Like> findAllByCommentId(Long id, Pageable pageable) {
        return repository.findAllByCommentId(id, pageable).map(mapper::toDomain);
    }

    @Override
    public Page<Like> findAllByPostId(Long id, Pageable pageable) {
        return repository.findAllByPostId(id, pageable).map(mapper::toDomain);
    }

    @Override
    public Like save(Like like) {
        return mapper.toDomain(manager.merge(mapper.toEntity(like)));
    }
}
