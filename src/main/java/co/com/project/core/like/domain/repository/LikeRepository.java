package co.com.project.core.like.domain.repository;

import co.com.project.core.like.domain.model.Like;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface LikeRepository {
    Page<Like> findAllByCommentId(Long id, Pageable pageable);
    Page<Like> findAllByPostId(Long id, Pageable pageable);
    Like save(Like like);
}
