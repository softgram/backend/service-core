package co.com.project.core.like.domain.exception;

import co.com.project.common.domain.exception.NotFoundException;
import lombok.experimental.StandardException;

@StandardException
public class NotFoundLikeByPostException extends NotFoundException {
    public NotFoundLikeByPostException(String message) {
        super("Not found like by post " + message + " in server");
    }
}
