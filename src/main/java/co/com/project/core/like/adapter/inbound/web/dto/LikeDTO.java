package co.com.project.core.like.adapter.inbound.web.dto;

import co.com.project.core.comment.adapter.inbound.web.dto.CommentDTO;
import co.com.project.core.post.adapter.inbound.web.dto.PostDTO;
import co.com.project.core.shared.adapter.inbound.web.dto.EntityBaseDTO;
import co.com.project.core.shared.adapter.inbound.web.dto.UserDTO;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder(builderMethodName = "init")
public class LikeDTO extends EntityBaseDTO {
    @Null
    private CommentDTO comment;
    @Null
    private PostDTO post;
    @NotNull
    private UserDTO user;
}
