package co.com.project.core.like.adapter.outbound.persistance.entity;

import co.com.project.core.comment.adapter.outbound.persistance.entity.Comment;
import co.com.project.core.post.adapter.outbound.persistance.entity.Post;
import co.com.project.core.shared.adapter.outbound.persistance.entity.EntityBase;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "LIKES")
public class Like extends EntityBase {
    @ManyToOne
    @JoinColumn(name = "POST_ID")
    private Post post;
    @ManyToOne
    @JoinColumn(name = "COMMENT_ID")
    private Comment comment;
    @Column(name = "USER_ID")
    private Long userId;
}
