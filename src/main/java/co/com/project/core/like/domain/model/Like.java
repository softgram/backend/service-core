package co.com.project.core.like.domain.model;

import co.com.project.core.comment.domain.model.Comment;
import co.com.project.core.post.domain.model.Post;
import co.com.project.core.shared.domain.model.Base;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder(builderMethodName = "init")
public class Like extends Base {
    private Comment comment;
    private Post post;
    private Long userId;
}
