package co.com.project.core.like.adapter.outbound.persistance.repository.jpa;

import co.com.project.core.like.adapter.outbound.persistance.entity.Like;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaLikeRepository extends JpaRepository<Like, Long> {
    Page<Like> findAllByPostId(Long id, Pageable pageable);
    Page<Like> findAllByCommentId(Long id, Pageable pageable);
}
