package co.com.project.core.message.domain.model;

import co.com.project.core.chat.domain.model.Chat;
import co.com.project.core.shared.domain.model.Base;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder(builderMethodName = "init")
public class Message extends Base {
    private String text;
    private Long userId;
    private Chat chat;
}
