package co.com.project.core.message.adapter.outbound.persistance.repository;

import co.com.project.core.message.adapter.outbound.mapper.MessageMapStructEntity;
import co.com.project.core.message.adapter.outbound.persistance.repository.jpa.JpaMessageRepository;
import co.com.project.core.message.domain.model.Message;
import co.com.project.core.message.domain.repository.MessageRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class MessageRepositoryImpl implements MessageRepository {
    @PersistenceContext private final EntityManager entityManager;
    private final JpaMessageRepository repository;
    private final MessageMapStructEntity mapper;

    @Override
    public Page<Message> findAllByChat(Long id, Pageable pageable) {
        return repository.findAllByChatId(id, pageable).map(mapper::toDomain);
    }

    @Override
    public Message save(Message message) {
        return mapper.toDomain(entityManager.merge(mapper.toEntity(message)));
    }
}
