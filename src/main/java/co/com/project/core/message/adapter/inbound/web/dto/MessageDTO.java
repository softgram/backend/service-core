package co.com.project.core.message.adapter.inbound.web.dto;

import co.com.project.core.chat.adapter.inbound.web.dto.ChatDTO;
import co.com.project.core.shared.adapter.inbound.web.dto.EntityBaseDTO;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder(builderMethodName = "init")
public class MessageDTO extends EntityBaseDTO {
    @NotNull
    @NotEmpty
    @NotBlank
    private String text;
    @NotNull
    private Long userId;
    @NotNull
    private ChatDTO chat;
}
