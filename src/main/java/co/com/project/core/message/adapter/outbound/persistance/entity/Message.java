package co.com.project.core.message.adapter.outbound.persistance.entity;

import co.com.project.core.chat.adapter.outbound.persistance.entity.Chat;
import co.com.project.core.shared.adapter.outbound.persistance.entity.EntityBase;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "MESSAGE")
public class Message extends EntityBase {
    @Column(name = "TEXT")
    private String text;
    @Column(name = "USER_ID")
    private Long userId;
    @ManyToOne
    @JoinColumn(name = "CHAT_ID")
    private Chat chat;
}
