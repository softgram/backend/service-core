package co.com.project.core.message.domain.exception;

import co.com.project.common.domain.exception.NotFoundException;
import lombok.experimental.StandardException;

@StandardException
public class NotMessageFoundException extends NotFoundException {
    public NotMessageFoundException(String message) {
        super("Not found messages for chat id ".concat(message));
    }
}
