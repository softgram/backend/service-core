package co.com.project.core.message.application.service;

import co.com.project.core.message.application.port.MessageUseCase;
import co.com.project.core.message.domain.model.Message;
import co.com.project.core.message.domain.repository.MessageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MessageService implements MessageUseCase {
    private final MessageRepository repository;

    @Override
    public Page<Message> findAllByChat(Long id, Pageable pageable) {
        return repository.findAllByChat(id, pageable);
    }

    @Override
    public Message save(Message message) {
        return repository.save(message);
    }
}
