package co.com.project.core.message.application.port;

import co.com.project.core.message.domain.model.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MessageUseCase {
    Page<Message> findAllByChat(Long id, Pageable pageable);
    Message save(Message message);
}
