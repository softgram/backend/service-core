package co.com.project.core.message.adapter.inbound.web.controller;

import co.com.project.common.adapter.inbound.web.dto.Response;
import co.com.project.core.message.adapter.inbound.mapper.MessageMapStructDTO;
import co.com.project.core.message.adapter.inbound.web.dto.MessageDTO;
import co.com.project.core.message.application.port.MessageUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import static co.com.project.common.adapter.util.ResponseBuilder.success;

@RestController
@RequiredArgsConstructor
@RequestMapping("/messages")
@CrossOrigin(originPatterns = {"http://localhost:4200"})
public class MessageController {
    private final MessageUseCase service;
    private final MessageMapStructDTO mapper;
    @GetMapping("/{id}")
    public Response<Page<MessageDTO>> findAllByChatId(@PathVariable Long id, Pageable pageable) {
        return success(service.findAllByChat(id, pageable).map(mapper::toDTO));
    }

    @PostMapping
    public Response<MessageDTO> saveMessage(@RequestBody MessageDTO message) {
        return success(mapper.toDTO(service.save(mapper.toDomain(message))));
    }
}
