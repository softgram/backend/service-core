package co.com.project.core.message.adapter.outbound.persistance.repository.jpa;

import co.com.project.core.message.adapter.outbound.persistance.entity.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaMessageRepository extends JpaRepository<Message, Long> {
    Page<Message> findAllByChatId(Long chatId, Pageable pageable);
}
