package co.com.project.core.message.adapter.outbound.mapper;

import co.com.project.core.message.domain.model.Message;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MessageMapStructEntity {
    Message toDomain(co.com.project.core.message.adapter.outbound.persistance.entity.Message message);
    co.com.project.core.message.adapter.outbound.persistance.entity.Message toEntity(Message message);
}
