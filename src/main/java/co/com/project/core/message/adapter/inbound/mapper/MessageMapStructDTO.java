package co.com.project.core.message.adapter.inbound.mapper;

import co.com.project.core.message.adapter.inbound.web.dto.MessageDTO;
import co.com.project.core.message.domain.model.Message;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MessageMapStructDTO {
    Message toDomain(MessageDTO message);
    @InheritInverseConfiguration(name = "toDomain")
    MessageDTO toDTO(Message message);
}
