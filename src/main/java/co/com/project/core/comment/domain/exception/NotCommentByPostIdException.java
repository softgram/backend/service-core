package co.com.project.core.comment.domain.exception;

import co.com.project.common.domain.exception.NotFoundException;
import lombok.experimental.StandardException;

@StandardException
public class NotCommentByPostIdException extends NotFoundException {
    public NotCommentByPostIdException(String message) {
        super("Not found comment by " + message + " in server.");
    }
}
