package co.com.project.core.comment.adapter.inbound.web.controller;

import co.com.project.common.adapter.inbound.web.dto.Response;
import co.com.project.core.comment.adapter.inbound.mapper.CommentMapStructDTO;
import co.com.project.core.comment.adapter.inbound.web.dto.CommentDTO;
import co.com.project.core.comment.application.port.CommentUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import static co.com.project.common.adapter.util.ResponseBuilder.success;

@RestController
@RequiredArgsConstructor
@RequestMapping("/comments")
@CrossOrigin(originPatterns = {"http://localhost:4200"})
public class CommentController {
    private final CommentUseCase service;
    private final CommentMapStructDTO mapper;

    @GetMapping("/{id}")
    public Response<Page<CommentDTO>> findAll(@PathVariable Long id, Pageable pageable) {
        return success(service.findAll(id, pageable).map(mapper::toDTO));
    }

    @PostMapping
    public Response<CommentDTO> save(@RequestBody CommentDTO comment) {
        return success(mapper.toDTO(service.save(mapper.toDomain(comment))));
    }
}
