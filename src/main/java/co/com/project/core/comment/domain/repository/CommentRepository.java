package co.com.project.core.comment.domain.repository;

import co.com.project.core.comment.domain.model.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CommentRepository {
    Page<Comment> findAll(Long postId, Pageable pageable);
    Comment save(Comment comment);
}
