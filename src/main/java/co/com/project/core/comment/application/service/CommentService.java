package co.com.project.core.comment.application.service;

import co.com.project.core.comment.application.port.CommentUseCase;
import co.com.project.core.comment.domain.model.Comment;
import co.com.project.core.comment.domain.repository.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CommentService implements CommentUseCase {
    private final CommentRepository repository;

    @Override
    public Page<Comment> findAll(Long postId, Pageable pageable) {
        return repository.findAll(postId, pageable);
    }

    @Override
    public Comment save(Comment comment) {
        return repository.save(comment);
    }
}
