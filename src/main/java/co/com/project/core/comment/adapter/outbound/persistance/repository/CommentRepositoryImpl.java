package co.com.project.core.comment.adapter.outbound.persistance.repository;

import co.com.project.core.comment.adapter.outbound.mapper.CommentMapStructEntity;
import co.com.project.core.comment.adapter.outbound.persistance.repository.jpa.JpaCommentRepository;
import co.com.project.core.comment.domain.exception.NotCommentByPostIdException;
import co.com.project.core.comment.domain.model.Comment;
import co.com.project.core.comment.domain.repository.CommentRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class CommentRepositoryImpl implements CommentRepository {
    @PersistenceContext private final EntityManager manager;
    private final JpaCommentRepository repository;
    private final CommentMapStructEntity mapper;

    @Override
    public Page<Comment> findAll(Long postId, Pageable pageable) {
        Page<Comment> posts = repository
                                .findAllByPostId(postId, pageable)
                                    .map(mapper::toDomain);

        if(posts.getTotalElements() == 0) throw new NotCommentByPostIdException(String.valueOf(postId));

        return posts;
    }

    @Override
    public Comment save(Comment comment) {
        return mapper.toDomain(manager.merge(mapper.toEntity(comment)));
    }
}
