package co.com.project.core.comment.adapter.inbound.mapper;

import co.com.project.core.comment.adapter.inbound.web.dto.CommentDTO;
import co.com.project.core.comment.domain.model.Comment;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CommentMapStructDTO {
    @Mapping(source = "user.id", target = "userId")
    Comment toDomain(CommentDTO comment);
    @Mapping(source = "userId", target = "user.id")
    @InheritInverseConfiguration(name = "toDomain")
    CommentDTO toDTO(Comment comment);
}
