package co.com.project.core.comment.application.port;

import co.com.project.core.comment.domain.model.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CommentUseCase {
    Page<Comment> findAll(Long postId, Pageable pageable);
    Comment save(Comment comment);
}
