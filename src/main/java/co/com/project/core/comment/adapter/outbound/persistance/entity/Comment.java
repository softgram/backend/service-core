package co.com.project.core.comment.adapter.outbound.persistance.entity;

import co.com.project.core.like.adapter.outbound.persistance.entity.Like;
import co.com.project.core.post.adapter.outbound.persistance.entity.Post;
import co.com.project.core.shared.adapter.outbound.persistance.entity.EntityBase;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "COMMENT")
public class Comment extends EntityBase {
    @Column(name = "VALUE")
    private String value;
    @Column(name = "USER_ID")
    private Long userId;
    @ManyToOne
    @JoinColumn(name = "POST_ID")
    private Post post;
    @OneToMany(mappedBy = "comment", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Like> likes;
}
