package co.com.project.core.comment.adapter.outbound.mapper;

import co.com.project.core.comment.domain.model.Comment;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CommentMapStructEntity {
    Comment toDomain(co.com.project.core.comment.adapter.outbound.persistance.entity.Comment comment);
    @InheritInverseConfiguration(name = "toDomain")
    co.com.project.core.comment.adapter.outbound.persistance.entity.Comment toEntity(Comment comment);
}
