package co.com.project.core.comment.domain.model;

import co.com.project.core.like.domain.model.Like;
import co.com.project.core.post.domain.model.Post;
import co.com.project.core.shared.domain.model.Base;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder(builderMethodName = "init")
public class Comment extends Base {
    private String value;
    private Long userId;
    private Post post;
    private List<Like> likes;
}
