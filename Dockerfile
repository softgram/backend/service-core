FROM openjdk:20 as builder

WORKDIR /app

COPY ./build/libs/service-core-1.0-SNAPSHOT.jar .

ENTRYPOINT java -jar ./service-core-1.0-SNAPSHOT.jar